#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libcollection/types.h>
#include <libcollection/compare.h>
#include <libcollection/list.h>
#include "libfiles.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void before_each();
void after_each();

void test_dir_exists();
void test_file_exists();
void test_file_move();
void test_files_all();
void test_files_all_recursive();
void test_files_like();
void test_files_like_recursive();
void test_files_by_extension();
void test_files_by_extension_recursive();
void test_file_prefix();
void test_file_suffix();
void test_file_is_path();
void test_file_info();
void test_directory_iterator();
void test_file_compare();

static FileInfo *first_file(const char *dir);

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("libfiles", argc, argv);
  test_before_each(before_each);
  test_after_each(after_each);

  test("Directory Exists", test_dir_exists);
  test("File Exists", test_file_exists);
  test("File Move", test_file_move);
  test("Files All", test_files_all);
  test("Files Like", test_files_like);
  test("Files by Extension", test_files_by_extension);
  test("Files All - Recursive", test_files_all_recursive);
  test("Files Like - Recursive", test_files_like_recursive);
  test("Files by Extension - Recursive", test_files_by_extension_recursive);
  test("File Prefix", test_file_prefix);
  test("File Suffix", test_file_suffix);
  test("File is Path", test_file_is_path);
  test("File Info", test_file_info);
	test("Directory Iterator", test_directory_iterator);
	test("File Compare", test_file_compare);

  return test_complete();
}

void before_each() {
  directory_new("testfiles");
}

void after_each() {
  directory_remove("testfiles");
}

void test_dir_exists() {
  directory_new("testfiles/testdir01");

  test_assert(directory_exists("testfiles/testdir01"), "Should return TRUE when directory exists");
  test_assert(!directory_exists("testfiles/ghost"), "Should return FALSE when directory does not exist");
}

void test_file_move() {
  file_touch("testfiles/testfile01");

  file_move("testfiles/testfile01", "testfiles/testfile02");
  test_assert(!file_exists("testfiles/testfile01"), "Should remove source file");
  test_assert(file_exists("testfiles/testfile02"), "Should add target file");
}

void test_file_exists() {
  file_touch("testfiles/testfile01");

  test_assert(file_exists("testfiles/testfile01"), "Should return TRUE when file exists");
  test_assert(!file_exists("testfiles/ghost"), "Should return FALSE when file does not exist");
}

int filename_compare(void *value, void *target) {
	FileInfo *file = (FileInfo *)value;
	char *filename = target;

	return string_compare(file->fullname, filename);
}

void test_files_all() {
  directory_new("testfiles/testdir01");
  file_touch("testfiles/testfile01");
  file_touch("testfiles/testfile02");
  file_touch("testfiles/nonce");

	// TODO -- Refactor into varargs function which takes the list and file names and performs the
	// assertions.
	List *files = files_all("testfiles", FALSE, TRUE);
	test_assert(list_contains(files, "nonce", filename_compare), "Should return all files within the directory");
	test_assert(list_contains(files, "testfile02", filename_compare), "Should return all files within the directory");
	test_assert(list_contains(files, "testfile01", filename_compare), "Should return all files within the directory");
	list_destroy(files);

	files = files_all("testfiles", FALSE, FALSE);
	test_assert(list_contains(files, "testfiles/nonce", filename_compare), "Should return all files within the directory");
	test_assert(list_contains(files, "testfiles/testfile02", filename_compare), "Should return all files within the directory");
	test_assert(list_contains(files, "testfiles/testfile01", filename_compare), "Should return all files within the directory");
	list_destroy(files);
}

void test_files_all_recursive() {
  file_touch("testfiles/testfile01");

  directory_new("testfiles/testdir01");
  file_touch("testfiles/testdir01/testfile02");
  file_touch("testfiles/testdir01/testfile03");

  directory_new("testfiles/testdir01/testsubdir01");
  file_touch("testfiles/testdir01/testsubdir01/testfile04");

  directory_new("testfiles/testdir02");
  file_touch("testfiles/testdir02/nonce");

	List *files = files_all("testfiles", TRUE, TRUE);
	test_assert(list_contains(files, "testdir02/nonce", filename_compare), "Should return all files within the sub-directory");
	test_assert(list_contains(files, "testfile01", filename_compare), "Should return all files within the directory");
	test_assert(list_contains(files, "testdir01/testsubdir01/testfile04", filename_compare), "Should return all files within the directory tree");
	test_assert(list_contains(files, "testdir01/testfile03", filename_compare), "Should return all files within the subdirectory");
	test_assert(list_contains(files, "testdir01/testfile02", filename_compare), "Should return all files within the subdirectory");
	list_destroy(files);
}

void test_files_like() {
  file_touch("testfiles/testfile01");
  file_touch("testfiles/testfile02");
  file_touch("testfiles/filetest01");

	List *files = files_like("testfiles", "testfile", FALSE, TRUE);
	test_assert(list_contains(files, "testfile02", filename_compare), "Should only return files that contain the query");
	test_assert(list_contains(files, "testfile01", filename_compare), "Should only return files that contain the query");
	list_destroy(files);

	files = files_like("testfiles", "testfile", FALSE, FALSE);
	test_assert(list_contains(files, "testfiles/testfile02", filename_compare), "Should only return files that contain the query with a prefix");
	test_assert(list_contains(files, "testfiles/testfile01", filename_compare), "Should only return files that contain the query with a prefix");
	list_destroy(files);

	files = files_like("testfiles", "nonce", FALSE, FALSE);
	test_assert_int(list_size(files), 0, "Should return an empty string if no files found");
	list_destroy(files);
}

void test_files_like_recursive() {
  file_touch("testfiles/testfile01");

  directory_new("testfiles/testdir01");
  file_touch("testfiles/testdir01/testfile02");
  file_touch("testfiles/testdir01/filetest03");

  directory_new("testfiles/testdir01/testsubdir01");
  file_touch("testfiles/testdir01/testsubdir01/testfile04");

  directory_new("testfiles/testdir02");
  file_touch("testfiles/testdir02/nonce");

	List *files = files_like("testfiles", "testfile", TRUE, TRUE);
	test_assert(list_contains(files, "testfile01", filename_compare), "Should return files containing 'testfile' within all subdirectories");
	test_assert(list_contains(files, "testdir01/testsubdir01/testfile04", filename_compare), "Should return files containing 'testfile' within all subdirectories");
	test_assert(list_contains(files, "testdir01/testfile02", filename_compare), "Should return files containing 'testfile' within all subdirectories");
	list_destroy(files);
}

void test_files_by_extension() {
  file_touch("testfiles/test01.txt");
  file_touch("testfiles/test02.c");
  file_touch("testfiles/test03.h");
  file_touch("testfiles/test04.txt");

	List *files = files_by_extension("testfiles", "txt", FALSE, TRUE);
	test_assert(list_contains(files, "test01.txt", filename_compare), "Should return all files with the extension");
	test_assert(list_contains(files, "test04.txt", filename_compare), "Should return all files with the extension");
	list_destroy(files);

	files = files_by_extension("testfiles", "txt", FALSE, FALSE);
	test_assert(list_contains(files, "testfiles/test01.txt", filename_compare), "Should return all files with the extension");
	test_assert(list_contains(files, "testfiles/test04.txt", filename_compare), "Should return all files with the extension");
	list_destroy(files);
}

void test_files_by_extension_recursive() {
  file_touch("testfiles/test01.txt");

  directory_new("testfiles/testdir01");
  file_touch("testfiles/testdir01/test02.txt");
  file_touch("testfiles/testdir01/test03.c");

  directory_new("testfiles/testdir01/testsubdir01");
  file_touch("testfiles/testdir01/testsubdir01/test04.txt");

  directory_new("testfiles/testdir02");
  file_touch("testfiles/testdir02/test05.h");

	List *files = files_by_extension("testfiles", "txt", TRUE, TRUE);
	test_assert(list_contains(files, "test01.txt", filename_compare), "Should return files with the '.txt' extension within all subdirectories");
	test_assert(list_contains(files, "testdir01/testsubdir01/test04.txt", filename_compare), "Should return files with the '.txt' extension within all subdirectories");
	test_assert(list_contains(files, "testdir01/test02.txt", filename_compare), "Should return files with the '.txt' extension within all subdirectories");
	list_destroy(files);
}

void test_file_prefix() {
  test_assert_string(file_prefix(NULL, "myfile", "/"), "myfile", "Should append nothing if prefix is NULL");
  test_assert_string(file_prefix("", "myfile", "/"), "myfile", "Should append nothing if prefix is empty");
  test_assert_string(file_prefix("mydir", "myfile", "/"), "mydir/myfile", "Should prefix filename with prefix and separator");
}

void test_file_suffix() {
  test_assert_string(file_suffix(NULL, "myfile", "."), "myfile", "Should append nothing if suffix is NULL");
  test_assert_string(file_suffix("", "myfile", "."), "myfile", "Should append nothing if suffix is empty");
  test_assert_string(file_suffix("txt", "myfile", "."), "myfile.txt", "Should suffix filename with extension and separator");
}

void test_file_is_path() {
  test_assert(!file_is_path("myfile"), "Should return false for files without a path");
  test_assert(!file_is_path("myfile.txt"), "Should return false for files without a path but with an extension");
  test_assert(file_is_path("mydir/myfile"), "Should return true for files with a path");
}

void test_file_info() {
  file_touch("testfiles/test01.txt");

  FileInfo *info;
  DIR *dir = opendir("testfiles");
  struct dirent *entry;
  while((entry = readdir(dir)) != NULL) {
    if(!strcmp(entry->d_name, "test01.txt")) {
      info = file_info("testfiles", entry);
    }
  }

  if(info == NULL) {
    test_fail("Should return a non-NULL file info");
    return;
  }

  test_assert_string(info->name, "test01", "Should extract the name");
  test_assert_string(info->extension, "txt", "Should extract the extension");
  test_assert_string(info->path, "testfiles", "Should extract the path");
  test_assert_string(info->fullname, "testfiles/test01.txt", "Should create a fullname with path, name and extension");
  test_assert(info->isFile, "Should identify entry as a file");
  test_assert(!info->isDirectory, "Should not identify entry as a directory");
  test_assert(info->hasExtension, "Should identify that the entry has a file extension");
}

void test_directory_iterator() {
	file_touch("testfiles/test01.txt");
	file_touch("testfiles/test02.txt");
	directory_new("testfiles/folder");
	file_touch("testfiles/folder/file.txt");

	int count = 0;
	int found1 = 0;
	int found2 = 0;
	int foundDir = 0;

	Iterator *iterator = directory_iterator("testfiles", "", FALSE);
	while(has_next(iterator)) {
		FileInfo *file = next_file(iterator);

		count++;
		if(!strcmp(file->fullname, "test01.txt")) {
			found1 = 1;
		}
		else if(!strcmp(file->fullname, "test02.txt")) {
			found2 = 1;
		}
		else if(!strcmp(file->fullname, "folder")) {
			foundDir = 1;
		}
	}

	iterator_destroy(iterator);
	test_assert(found1, "Should iterate over first file");
	test_assert(found2, "Should iterate over second file");
	test_assert(foundDir, "Should iterate over the directory");
	test_assert_int(count, 3, "Should only iterate three times");
}

void test_file_compare() {
	file_touch("testfiles/test01.txt");
	FileInfo *file1 = first_file("testfiles");
	FileInfo *file2 = first_file("testfiles");

	test_assert_int(file_compare(file1, NULL), LESS, "Should return less than when checked against NULL");
	test_assert_int(file_compare(file1, file2), EQUAL, "Should return equal when files are the same");
}

static FileInfo *first_file(const char *dir) {
	Iterator *iterator = directory_iterator(dir, dir, FALSE);
	has_next(iterator);

	FileInfo *file1 = next_file(iterator);
	iterator_destroy(iterator);

	return file1;
}
