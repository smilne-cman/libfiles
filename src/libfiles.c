#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <libcollection/iterator.h>
#include <libcollection/compare.h>
#include <libcollection/types.h>
#include <libcollection/list.h>
#include <libstring/libstring.h>
#include "libfiles.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static int query_is_file(FileInfo *file, void *arg);
static int query_filename_contains(FileInfo *file, void *arg);
static int query_by_extension(FileInfo *file, void *arg);
static int string_ends_with(const char *str, const char *suffix);

static void *directory_iterator_next(Iterator *iterator);
static int directory_iterator_has_next(Iterator *iterator);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void file_touch(const char *filename) {
  char *command = (char *)malloc(sizeof(char) * 256);
  sprintf(command, "touch %s", filename);

  system(command);
}

int file_exists(const char *filename) {
  FILE *file = fopen(filename, "r");

  if(file){
    fclose(file);
    return TRUE;
  }

  return FALSE;
}

void file_move(const char *source, const char *target) {
  if(file_exists(source)) {
    char *command = (char *)malloc(sizeof(char) * 1024);
    sprintf(command, "mv %s %s", source, target);
    system(command);
    free(command);
  }
}

List *files_query(const char *path, FileQuery query, void *arg, int recursive, int relative) {
	List *files = list_new(ListAny);

  Iterator *iterator = directory_iterator(path, relative ? "" : path, recursive);
  while(has_next(iterator)) {
    FileInfo *file = next_file(iterator);

    if(query(file, arg) == FALSE) continue;

    list_add(files, file);
  }

  iterator_destroy(iterator);
  return files;
}

List *files_all(const char *path, int recursive, int relative) {
  return files_query(path, query_is_file, NULL, recursive, relative);
}

List *files_by_extension(const char *path, const char *extension, int recursive, int relative) {
  return files_query(path, query_by_extension, extension, recursive, relative);
}

List *files_like(const char *path, const char *query, int recursive, int relative) {
  return files_query(path, query_filename_contains, query, recursive, relative);
}

int directory_exists(const char *path) {
  DIR *dir = opendir(path);

  if(dir) {
    closedir(dir);
    return TRUE;
  }

  return FALSE;
}

void directory_new(const char *name) {
  if(directory_exists(name)) return;

  char *command = (char *)malloc(sizeof(char) * 256);
  sprintf(command, "mkdir %s", name);

  system(command);
}

void directory_remove(const char *name) {
  char *command = (char *)malloc(sizeof(char) * 256);
  sprintf(command, "rm -fr %s", name);

  system(command);
}

char *file_prefix(const char *prefix, const char *filename, const char *separator) {
  char *qualifiedFilename = (char *)malloc(sizeof(char) * 1024);

  if(prefix == NULL || strcmp(prefix, "") == EQUAL) {
    sprintf(qualifiedFilename, "%s", filename);
  } else {
    sprintf(qualifiedFilename, "%s%s%s", prefix, separator, filename);
  }

  return qualifiedFilename;
}

char *file_suffix(const char *suffix, const char *filename, const char *separator) {
  char *qualifiedFilename = (char *)malloc(sizeof(char) * 1024);

  if(suffix == NULL || strcmp(suffix, "") == EQUAL) {
    sprintf(qualifiedFilename, "%s", filename);
  } else {
    sprintf(qualifiedFilename, "%s%s%s", filename, separator, suffix);
  }

  return qualifiedFilename;
}


int file_is_path(const char *filename) {
  return strstr(filename, "/") != NULL;
}

FileInfo *file_info(const char *path, struct dirent *entry) {
  FileInfo *info = (FileInfo *)malloc(sizeof(FileInfo));
  int extension = string_last_index(entry->d_name, '.');

  info->path = path;

  if (extension != -1) {
    info->name = substring(entry->d_name, 0, extension - 1);
    info->extension = substring(entry->d_name, extension + 1, -1);
  } else {
    info->name = entry->d_name;
    info->extension = NULL;
  }

  info->fullname = file_prefix(path, entry->d_name, "/");
  info->isDirectory = entry->d_type == DT_DIR;
  info->isFile = entry->d_type = DT_REG;
  info->hasExtension = info->extension != NULL;

  return info;
}

static int query_is_file(FileInfo *file, void *arg) {
  return file->isFile && !file->isDirectory;
}

static int query_filename_contains(FileInfo *file, void *arg) {
  if(!file->isFile) return FALSE;

  return strstr(file->name, arg) != NULL;
}

static int query_by_extension(FileInfo *file, void *arg) {
  if(!file->isFile || !file->hasExtension) return FALSE;

  return string_ends_with(file->extension, arg);
}

// TODO -- Move to utility library
static int string_ends_with(const char *str, const char *suffix) {
  int str_len = strlen(str);
  int suffix_len = strlen(suffix);

  return
    (str_len >= suffix_len) &&
    (0 == strcmp(str + (str_len-suffix_len), suffix));
}

typedef struct DirectoryIteratorData {
  int recursive;
  const char *prefix;
  const char *path;
  FileInfo *next;
  Iterator *subiterator;
} DirectoryIteratorData;

Iterator *directory_iterator(const char *path, const char *prefix, int recursive) {
  DIR *dir = opendir(path);

  DirectoryIteratorData *data = (DirectoryIteratorData *)malloc(sizeof(DirectoryIteratorData));
  data->recursive = recursive;
  data->prefix = prefix;
  data->path = path;
  data->next = NULL;
  data->subiterator = NULL;

  return iterator_new(dir, directory_iterator_next, directory_iterator_has_next, data);
}

FileInfo *next_file(Iterator *iterator) {
  return (FileInfo *)next(iterator);
}

static void *directory_iterator_next(Iterator *iterator) {
  DirectoryIteratorData *data = (DirectoryIteratorData *)iterator->meta;

  if(data->recursive && data->subiterator != NULL) {
    return next(data->subiterator);
  }

  return data->next;
}

static int directory_iterator_has_next(Iterator *iterator) {
  if(iterator->collection == NULL) return FALSE;

  DirectoryIteratorData *data = (DirectoryIteratorData *)iterator->meta;

  if(data->recursive && data->next != NULL && data->next->isDirectory) {
    data->subiterator = directory_iterator(
      file_prefix(data->path, data->next->name, "/"),
      file_prefix(data->prefix, data->next->name, "/"),
      TRUE
    );
    data->next = NULL;
  }

  if(data->recursive && data->subiterator != NULL) {
    if(has_next(data->subiterator)) {
      return TRUE;
    } else {
      iterator_destroy(data->subiterator);
      data->subiterator = NULL;
    }
  }

  // Skip over . and .. directory entries
  struct dirent *entry = readdir((DIR *)iterator->collection);
  while(entry != NULL && (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))) {
    entry = readdir((DIR *)iterator->collection);
  }

  if(entry == NULL) {
    data->next = NULL;
    closedir((DIR *)iterator->collection);
    return FALSE;
  }

  data->next = file_info(data->prefix, entry);
  return TRUE;
}

int file_compare(void *value, void *target) {
	if (value == NULL || target == NULL) return LESS;

	FileInfo *file1 = (FileInfo *)value;
	FileInfo *file2 = (FileInfo *)target;

	return string_compare(file1->fullname, file2->fullname);
}
