#ifndef __libfiles__
#define __libfiles__

/* Includes *******************************************************************/
#include <dirent.h>
#include <libcollection/iterator.h>
#include <libcollection/list.h>

/* Types **********************************************************************/
typedef struct FileInfo {
  const char *name;
  const char *path;
  const char *extension;
  const char *fullname;
  int hasExtension;
  int isDirectory;
  int isFile;
} FileInfo;

typedef int (*FileQuery)(FileInfo *file, void *query);

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void file_touch(const char *filename);
int file_exists(const char *filename);
void file_move(const char *filename, const char *newFilename);
List *files_query(const char *path, FileQuery query, void *arg, int recursive, int relative);

List *files_all(const char *path, int recursive, int relative);
List *files_by_extension(const char *path, const char *extension, int recursive, int relative);
List *files_like(const char *path, const char *query, int recursive, int relative);

int directory_exists(const char *path);
void directory_new(const char *name);
void directory_remove(const char *name);

Iterator *directory_iterator(const char *path, const char *prefix, int recursive);
FileInfo *next_file(Iterator *iterator);

char *file_prefix(const char *prefix, const char *filename, const char *separator);
char *file_suffix(const char *suffix, const char *filename, const char *separator);

int file_is_path(const char *filename);
FileInfo *file_info(const char* path, struct dirent *entry);
int file_compare(void *value, void *target);

#endif
